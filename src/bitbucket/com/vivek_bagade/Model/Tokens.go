package Model

import (
	"database/sql"
)

type Tokens struct {
	CountryCode int            `db:"countryCode"`
	PhoneNumber int64          `db:"phoneNumber"`
	AuthToken   sql.NullString `db:"authToken"`
	UserName    sql.NullString `db:"userName"`
}
