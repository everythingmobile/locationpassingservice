package Model

import "database/sql"

type MeetUp struct {
	TableId                int            `db:"tableId"`
	MeetUpId               sql.NullString `db:"meetUpId"`
	ParticipantCountryCode int            `db:"participantCountryCode"`
	ParticipantPhoneNumber sql.NullInt64  `db:"participantPhoneNumber"`
	StartUpTime            []uint8        `db:"startUpTime"`
	IsParticipantAdmin     sql.NullString `db:"isParticipantAdmin"`
	DestinationLatitude    sql.NullString `db:"destinationLatitude"`
	DestinationLongitude   sql.NullString `db:"destinationLongitude"`
	LocationDescription    sql.NullString `db:"locationDescription"`
    LocationAddress        sql.NullString `db:"locationAddress"`
}
