package Model

type InitMessage struct {
	MessageHead string
	MeetUpId string
	UserToken string
	UserCountryCode int
	UserPhoneNumber int64
}

