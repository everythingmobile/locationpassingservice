package LocService

import (
	"bitbucket/com/vivek_bagade/Model"
	"bitbucket/com/vivek_bagade/Service"
	"database/sql"
	"encoding/json"
	"errors"
	"fmt"
	"github.com/gorilla/websocket"
	"time"
)

const (
	// Time allowed to write a message to the peer.
	writeWait = 10 * time.Second
	// Maximum message size allowed from peer.
	maxMessageSize = 512
	maxInitTimeInSec = 50
)

type MeetUpWebSocketConnection struct {
	Ws   *websocket.Conn
	Send chan []byte
}

type MeetUpHubChannel struct {
	meetUpId          string
	receiveHubChannel chan *MeetUpWebSocketHub
}

func (c *MeetUpWebSocketConnection) StartReading(appropriateHub *MeetUpWebSocketHub) {
	defer func() {
		fmt.Println("stoping reads")
		appropriateHub.unregister <- c
		c.Ws.Close()
	}()

	for {
		_, message, err := c.Ws.ReadMessage()
		if err != nil {
			break
		}
		fmt.Println("Message received: " + string(message))
		if hubShouldClose, _ := isCloseMessage(message); hubShouldClose == true {
			fmt.Println("got message to close hub")
			hubFactory.deleteHubChannel <- appropriateHub.meetUpId
			break
		}

		appropriateHub.broadcast <- message

	}
}

func (c *MeetUpWebSocketConnection) write(mt int, payload []byte) error {
	c.Ws.SetWriteDeadline(time.Now().Add(writeWait))
	return c.Ws.WriteMessage(mt, payload)
}

func (c *MeetUpWebSocketConnection) PushSelfToAppropriateHubAndStart() {

	c.Ws.SetReadLimit(maxMessageSize)

	var appropriateMeetUpId string
	var err error
	for {
		c.Ws.SetReadDeadline(time.Now().Add(maxInitTimeInSec * time.Second))
		message := c.getFirstMessage()
		if string(message) == "Timeout" {
			fmt.Println("Closing webSocket")
			c.Close()
			return;
		}
		if hubShouldClose, _ := isCloseMessage(message); hubShouldClose == true {
			fmt.Println("closed before assigning hub")
			c.Close()
			return
		}
		appropriateMeetUpId, err = c.verifyFirstMessage(message)
		if err != nil {
			fmt.Println(err)
			fmt.Println("Cant get Id for meetup")
		} else {
			c.Ws.SetReadDeadline(time.Time{})
			break;
		}
	}

	meetUpHubChannel := MeetUpHubChannel{meetUpId: appropriateMeetUpId, receiveHubChannel: make(chan *MeetUpWebSocketHub)}
	hubFactory.getHubChannel <- &meetUpHubChannel
	appropriateMeetUpHub := <-meetUpHubChannel.receiveHubChannel
	appropriateMeetUpHub.register <- c

	fmt.Println("Everything done")
	go c.StartWriting(appropriateMeetUpHub)
	c.StartReading(appropriateMeetUpHub)
}

func (c *MeetUpWebSocketConnection) getFirstMessage() []byte {
	_, message, err := c.Ws.ReadMessage()
	if err != nil {
		fmt.Println(err)
		return []byte("Timeout")
	}

	fmt.Println("Message received:%v", message)

	return message
}

func isCloseMessage(message []byte) (bool, error) {
	var closeMessage CloseMessage
	parseErr := json.Unmarshal(message, &closeMessage)
	if parseErr != nil {
		return false, errors.New("Cant parse")
	}
	if closeMessage.MessageHead == "close" {
		return true, nil
	}
	return false, nil
}

func (c *MeetUpWebSocketConnection) Close() {
	close(c.Send)
	c.Ws.Close()
}

func (c *MeetUpWebSocketConnection) verifyFirstMessage(message []byte) (string, error) {
	var initMessage Model.InitMessage
	parseErr := json.Unmarshal(message, &initMessage)
	if parseErr != nil {
		return "", errors.New("Cant parse")
	}
	if initMessage.MessageHead == "init" {

		isAuthenticated, autherr := Service.VerifyAuthTokens(Model.Tokens{PhoneNumber: initMessage.UserPhoneNumber,CountryCode: initMessage.UserCountryCode, AuthToken: sql.NullString{initMessage.UserToken, true}})
		if autherr != nil {
			fmt.Println(autherr)
			return "", autherr
		}
		isAppMeetUp, meetUpErr := Service.VerifyMeetUp(initMessage)
		if meetUpErr != nil {
			fmt.Println(meetUpErr)
			return "", meetUpErr
		}
		if isAuthenticated == true && isAppMeetUp == true {
			fmt.Println("%v %v", isAuthenticated, isAppMeetUp)
			return initMessage.MeetUpId, nil
		}

	}
	return "", errors.New("Cant read meetupid")
}

func (c *MeetUpWebSocketConnection) StartWriting(appropriateMeetUpHub *MeetUpWebSocketHub) {
	defer func() {
		fmt.Println("closing write routine")
		c.Ws.Close()
	}()
	for {
		select {
		case message, ok := <-c.Send:
			if !ok {
				fmt.Println("stop writing")
				c.write(websocket.CloseMessage, []byte{})
				return
			}
			if err := c.write(websocket.TextMessage, message); err != nil {
				return
			}

		}

	}

}
