package LocService
import "fmt"

type HubFactory struct {

	hubMap map[string] *MeetUpWebSocketHub
	getHubChannel chan *MeetUpHubChannel 
	deleteHubChannel chan string
	
	sendHubFactory chan *HubFactory
}

var hubFactory *HubFactory

func GetHubFactory() *HubFactory {
	if hubFactory == nil {
		fmt.Println("creating new hubFactory")
		hubFactory = new(HubFactory)
		hubFactory.hubMap = make(map[string]*MeetUpWebSocketHub)
		hubFactory.getHubChannel= make(chan *MeetUpHubChannel)
		hubFactory.deleteHubChannel=make(chan string)
		
	}
	return hubFactory
}
func RunHubFactory() {
	
	for {
			select{
			case hubChannel:= <- hubFactory.getHubChannel:
				
				wsHub := getHub(hubFactory,hubChannel.meetUpId)
				hubFactory.hubMap[hubChannel.meetUpId]=wsHub
				hubChannel.receiveHubChannel<- wsHub

			case meetUpId:= <- hubFactory.deleteHubChannel:
				wsHub:=hubFactory.hubMap[meetUpId]
				wsHub.closeHub<-true
				delete(hubFactory.hubMap,meetUpId)
			}
		}
}
func makeNewHub(meetUpId string) *MeetUpWebSocketHub {
	var ret *MeetUpWebSocketHub = new(MeetUpWebSocketHub)
	ret.broadcast = make(chan []byte)
	ret.register = make(chan *MeetUpWebSocketConnection)
	ret.unregister = make(chan *MeetUpWebSocketConnection)
	ret.connections = make(map[*MeetUpWebSocketConnection]bool)
	ret.closeHub=make(chan bool)
	ret.meetUpId= meetUpId
	go ret.Run()
	return ret
}

func getHub(factory *HubFactory, meetUpId string) *MeetUpWebSocketHub {
	if _, ok := factory.hubMap[meetUpId]; !ok {
		factory.hubMap[meetUpId] = makeNewHub(meetUpId)
	}
	return factory.hubMap[meetUpId]
}



