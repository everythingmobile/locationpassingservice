package LocService

type MeetUpWebSocketHub struct {
	// Registered connections.
	connections map[*MeetUpWebSocketConnection]bool

	// Inbound messages from the connections.
	broadcast chan []byte

	meetUpId string

	// Register requests from the connections.
	register chan *MeetUpWebSocketConnection

	// Unregister requests from connections.
	unregister chan *MeetUpWebSocketConnection
	closeHub chan bool 
}

func (h *MeetUpWebSocketHub) Run() {

	for {
		select {
		case c := <-h.register:
			h.connections[c] = true
		case c := <-h.unregister:
			delete(h.connections, c)
			close(c.Send)
		case m := <-h.broadcast:
			for c := range h.connections {
				select {
				case c.Send <- m:
				default:
					close(c.Send)
					delete(h.connections, c)
				}
			}
		case <-h.closeHub:
			h.unregister = nil
			h.register=nil
			h.broadcast =nil
			for c := range h.connections {
					close(c.Send)
					delete(h.connections, c)

			}
			return
		}
	}
}
