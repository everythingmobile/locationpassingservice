package Service

import (
	"bitbucket/com/vivek_bagade/Model"
	"fmt"
	"errors"
)

func VerifyAuthTokens(authTokenObj Model.Tokens) (bool, error) {
	if authTokenObj.AuthToken.String != "" {
		obj, err := DbConn.Get(Model.Tokens{}, authTokenObj.CountryCode, authTokenObj.PhoneNumber)
		if err != nil {
			return false, err
		}
		if obj == nil {
			return false, nil
		}
		token := obj.(*Model.Tokens)
		if token.AuthToken.Valid == true && token.AuthToken.String == authTokenObj.AuthToken.String {
			return true, nil
		}

	}
	return false, errors.New("not verified")
}

func VerifyMeetUp(initMessage Model.InitMessage) (bool, error) {
	var meetUp Model.MeetUp
	meetUpId := initMessage.MeetUpId
	phoneNumber := initMessage.UserPhoneNumber
	countryCode := initMessage.UserCountryCode
	if meetUpId != "" {
		err := DbConn.SelectOne(&meetUp, "select * from meetups where meetUpId=? and participantPhoneNumber=? and participantCountryCode=?", meetUpId, phoneNumber, countryCode)
		fmt.Println(meetUp)
		fmt.Println(err)
		if err != nil {
			return false, nil
		}
		if meetUp.MeetUpId.Valid == true && meetUp.ParticipantPhoneNumber.Valid == true && meetUp.MeetUpId.String == meetUpId && meetUp.ParticipantCountryCode == countryCode && meetUp.ParticipantPhoneNumber.Int64 == phoneNumber {
			return true, nil
		}

	}
	return false, errors.New("cant verify meetup")

}
