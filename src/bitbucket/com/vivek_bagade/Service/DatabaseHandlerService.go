package Service

import (
	"bitbucket/com/vivek_bagade/Model"
	"database/sql"
	"github.com/coopernurse/gorp"
	_ "github.com/go-sql-driver/mysql"
)

var DbConn *gorp.DbMap

func InitDb() (*gorp.DbMap, error) {
	db, err := sql.Open("mysql", "root:bitrush@2015@tcp(localhost:3306)/meetup_schema")
	if err != nil {
		return nil, err
	}
	dbmap := &gorp.DbMap{Db: db, Dialect: gorp.MySQLDialect{"InnoDB", "UTF8"}}
	dbmap.AddTableWithName(Model.Tokens{}, "tokens").SetKeys(false, "countryCode", "phoneNumber")
	dbmap.AddTableWithName(Model.MeetUp{}, "meetups").SetKeys(true, "tableId")
	DbConn = dbmap
	return dbmap, nil
}
