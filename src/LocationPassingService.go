package main

import (
	"bitbucket/com/vivek_bagade/LocService"
	"bitbucket/com/vivek_bagade/Service"
	"log"
	"net/http"
	"fmt"
	"github.com/gorilla/websocket"
)

func main() {
	dbmap, err := Service.InitDb()
	if err != nil {
		log.Fatal("Could not create database connection: ", err)
		return
	}
	defer dbmap.Db.Close()
	LocService.GetHubFactory()
	go LocService.RunHubFactory()
	http.HandleFunc("/ws", ServeWs)
	err = http.ListenAndServe("ec2-52-74-140-186.ap-southeast-1.compute.amazonaws.com:4000", nil)
	if err != nil {
		log.Fatal("ListenAndServe: ", err)
	}
}

var upgrader = websocket.Upgrader{
ReadBufferSize:  1024,
WriteBufferSize: 1024,
CheckOrigin:     CheckOrigin,
}

func CheckOrigin(r *http.Request) bool {
	return true
}

func ServeWs(w http.ResponseWriter, r *http.Request) {
	if r.Method != "GET" {
		http.Error(w, "Method not allowed", 405)
		return
	}
	ws, err := upgrader.Upgrade(w, r, nil)

	fmt.Println("Received connection")
	if err != nil {
		fmt.Println(err)
		if _, ok := err.(websocket.HandshakeError); !ok {
			log.Println(err)
		}
		return
	}
	c := &LocService.MeetUpWebSocketConnection{Send: make(chan []byte, 256), Ws: ws}
	c.PushSelfToAppropriateHubAndStart()
}
