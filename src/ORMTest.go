package main

import (
	"database/sql"
	"github.com/coopernurse/gorp"
	"bitbucket/com/vivek_bagade/Model"
	"fmt"
	_ "github.com/ziutek/mymysql/godrv"
)

func main() {
	db, err := sql.Open("mymysql", "tcp:localhost:3306*hibernate/root/bagsER1992")
	if err!=nil {
		panic(err)
	}
	dbmap := &gorp.DbMap{Db: db, Dialect: gorp.MySQLDialect{"InnoDB", "UTF8"}}

	dbmap.AddTableWithName(Model.Tokens{}, "tokens").SetKeys(false, "phoneNumber")

	token, err := dbmap.Get(Model.Tokens{}, 917406444421)

	var tokens []Model.Tokens

	if err!=nil {
		panic(err)
	}

	_, _ = dbmap.Select(&tokens, "select * from tokens order by phoneNumber")

	fmt.Println(token)
	fmt.Println(tokens)

}
